
# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.7.0] - 2022-09-27

#### Enhancements

- [#23809] Passed to grsf-common-library.2.0.0[-SNAPSHOT]
- [#23811#note-6] Passed to GWT_2.9

## [v1.6.1] - 2022-06-22

#### Bug fixes

- [#23549] Fixed serialization issue on the GRSFRecordAlreadyManagedStatusException
- [#23561] Fixed Merging Request throws a Null Pointer exception

## [v1.6.0] - 2022-05-25

#### Bug fixes

- [#23408] GRSF Manage widget is buggy

#### Enhancements

- [#23407] Social Post: replace the BR tags with new lines 

## [v1.5.0] - 2021-04-12

[#21153] Upgrade the maven-portal-bom to 3.6.1 version
[#20728] Migrate to catalogue-util-library


## [v1.4.2-SNAPSHOT] - 2021-02-17

[#19500] Migrate to git/jenkins


## [v1.4.1] - 2020-18-06

[#19500] Migrate to git/jenkins


## [v1.0.0] - 2017-26-01

First release
